// Julián Terán Vázquez (a329748)

// 1. Ejecute el siguiente comando:
mongoimport -d students -c grades < grades.json
use students

// 2. ¿Cuántos registros arrojo el comando count?
db.grades.count()
// Resultado: 800

// 3. Encuentra todas las calificaciones del estudiante con el id numero 4:
db.grades.find({"student_id":4}, {_id:0, score:1})
/* Resultado:
{ "score" : 87.89071881934647 }
{ "score" : 5.244452510818443 }
{ "score" : 27.29006335059361 }
{ "score" : 28.656451042441 }*/

// 4. ¿Cuántos registros hay de tipo exam?:
db.grades.find({"type":"exam"}).count()
// Resultado: 200

// 5. ¿Cuántos registros hay de tipo homework?
db.grades.find({"type":"homework"}).count()
// Resultado: 400

// 6. ¿Cuántos registros hay de tipo quiz?
db.grades.find({"type":"quiz"}).count()
// Resultado: 200

// 7. Elimina todas las calificaciones del estudiante con el id numero 3:
db.grades.deleteMany({"student_id":3})
{ "acknowledged" : true, "deletedCount" : 4 }
db.grades.find({"student_id":3})
// Resultado: *las calificaciones son eliminadas*

// 8. ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea?:
db.grades.find({"score":75.29561445722392,"type":"homework"}, {_id:0})
// Resultado:
// { "student_id" : 9, "type" : "homework", "score" : 75.29561445722392 }

// 9. Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100:
db.grades.update({"_id":ObjectId("50906d7fa3c412bb040eb591")}, {$set:{"score":100}})
// Resultado:
// WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })

// 10. ¿A qué estudiante pertenece esta calificación?:
db.grades.find({"_id": ObjectId("50906d7fa3c412bb040eb591")}, {type:0})
// Resultado:
// { "_id" : ObjectId("50906d7fa3c412bb040eb591"), "student_id" : 6, "score" : 100 }
